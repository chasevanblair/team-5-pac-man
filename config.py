import os

import pygame.font

pygame.font.init()
pygame.mixer.init()

YELLOW = (255, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)

MAX_ROWS = 23
MAX_COLS = 23
clock = pygame.time.Clock()
VEL = 3
WIDTH, HEIGHT = 1500, 900

PACMAN_INTRO_SOUND = pygame.mixer.Sound(os.path.join('Sounds', 'pacman_beginning.wav'))
PACMAN_CHOMP_SOUND = pygame.mixer.Sound(os.path.join('Sounds', 'pacman_chomp.wav'))
PACMAN_DEATH_SOUND = pygame.mixer.Sound(os.path.join('Sounds', 'pacman_death.wav'))
PACMAN_EAT_GHOST_SOUND = pygame.mixer.Sound(os.path.join('Sounds', 'pacman_eatghost.wav'))



GAME_SIZE = 700
GRID_DIMENSIONS = GAME_SIZE / MAX_ROWS
GAME_PADDING = 100
WIN = pygame.display.set_mode((WIDTH, HEIGHT))

PACMAN_DIMENSIONS = GRID_DIMENSIONS
PELLET_WIDTH, PELLET_HEIGHT = GRID_DIMENSIONS, GRID_DIMENSIONS

FPS = 60

TEXT_FONT = pygame.font.SysFont('Agency FB', 40)
#LIVES_FONT = pygame.font.SysFont('comicsans', 40)


