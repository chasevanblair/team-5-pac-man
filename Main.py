import random
import os
import pygame
import pygame_menu
from pathfinding.core.grid import Grid
from pygame import font

from config import *
from map_layouts import *

pygame.font.init()
pygame.init()
pygame.mixer.init()
ghost_status = pygame.USEREVENT + 0
power_down = pygame.USEREVENT + 2
GAME_SURFACE = pygame.surface.Surface((GAME_SIZE, GAME_SIZE + GRID_DIMENSIONS))
GAME_SURFACE_X = 0
GAME_SURFACE_Y = 0
wall_group = pygame.sprite.Group()
pellet_group = pygame.sprite.Group()
power_pellet_group = pygame.sprite.Group()
cols = 24

# all ghosts start inactive then get moved to active as released
active_ghost_group = pygame.sprite.Group()
inactive_ghosts_group = pygame.sprite.Group()

pacman_group = pygame.sprite.Group()
ghost_spawn_location = []
level_layout = [
    'WWWWWWWWWWWWWWWWWWWW',
    'WU        W       UW',
    'W WW WWWW W WWW WW W',
    'W WW WWWW W WWW WW W',
    'W                  W',
    'W WW W WWWWWWW W W W',
    'W    W     W   W W W',
    'WWWW WWWWW W WWW W W',
    'bbbW W    X    W W W',
    'bbbW W WWWWWWW W W W',
    'WWWW W WGbGbGW W W W',
    'T      WbbbbbW     T',
    'WWWW W WWWWWWW W WWW',
    'bbbW W    P    W Wbb',
    'bbbW W WWWWWWW W Wbb',
    'WWWW W WWWWWWW W WWW',
    'W         W        W',
    'W WW WWWW W WWW WW W',
    'W  W            WW W',
    'WW W WWWW W WWW WW W',
    'W    WWWW W WWW    W',
    'W WWWWWWW W WWWWWW W',
    'WU                UW',
    'WWWWWWWWWWWWWWWWWWWW'
]


class Wall(pygame.sprite.Sprite):
    # TODO move to sprite group not list
    def __init__(self, pos):
        super().__init__()
        self.image = pygame.image.load("images/wall.png")
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]


class Pellet(pygame.sprite.Sprite):
    # TODO move to sprite group not list
    def __init__(self, pos):
        super().__init__()
        self.image = pygame.image.load("images/pellet.png")
        self.image = pygame.transform.scale(self.image, (PELLET_WIDTH, PELLET_HEIGHT))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]


class Pacman(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.image = pygame.image.load("images/anim/pacman_0.png")
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.angle = 0
        self.powered_up = False
        self.sprites = []
        self.sprites_power = []
        self.current_sprite = 0

        self.sprites.append(pygame.image.load("images/anim/pacman_0.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_1.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_2.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_3.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_4.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_3.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_2.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_1.png"))
        self.sprites.append(pygame.image.load("images/anim/pacman_0.png"))

        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_0.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_1.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_2.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_3.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_4.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_3.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_2.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_1.png"))
        self.sprites_power.append(pygame.image.load("images/anim/pacman_p_0.png"))

    def power_up(self):
        #TODO sync new image to current rotation
        print("powered up")
        self.powered_up = True
        self.image = self.sprites_power[int(self.current_sprite)]
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.image = pygame.transform.rotate(self.image, self.angle)
        for ghost in active_ghost_group:
            ghost.scared = True
            ghost.image = pygame.image.load("images/ghost_scared.png")
            ghost.image = pygame.transform.scale(ghost.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        pygame.time.set_timer(power_down, 5000, 1)

    def rotate(self, angle):
        # if powered up use other image
        if not self.powered_up:
            self.image = self.sprites[int(self.current_sprite)]
        else:
            self.image = self.sprites_power[int(self.current_sprite)]
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.image = pygame.transform.rotate(self.image, angle)
        self.angle = angle

    def update(self):
        self.current_sprite += .3
        if self.current_sprite >= len(self.sprites_power):
            self.current_sprite = 0
        if self.powered_up:
            self.image = self.sprites_power[int(self.current_sprite)]
        else:
            self.image = self.sprites[int(self.current_sprite)]
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.image = pygame.transform.rotate(self.image, self.angle)


class Power_pellet(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.image = pygame.image.load("images/power_pellet.png")
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]


class Ghost(pygame.sprite.Sprite):
    # self.filename
    # self.pathing
    # self.ghostname
    activated = False
    spawn_location = (0, 0)
    x_vel = 0
    y_vel = 0

    def __init__(self, pos):
        super().__init__()
        self.image = pygame.image.load("images/ghost.png")
        self.image = pygame.transform.scale(self.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.scared = False
        self.initial_x = pos[0]
        self.initial_y = pos[1]
        # inactive_ghosts_group.add(self)

    def cycle_activation(self):
        if not self.activated:
            activated = True
            self.rect.x = self.spawn_location[0]
            self.rect.y = self.spawn_location[1]
            active_ghost_group.add(self)
            #self.x_vel, self.y_vel = self.get_random_direction()
            self.get_random_direction()

        elif self.activated:
            activated = False
            active_ghost_group.remove(self)
            inactive_ghosts_group.add(self)

    def update(self) -> None:
        self.rect.x += self.x_vel
        self.rect.y += self.y_vel

    def set_spawn(self, spawn_location_map):
        self.spawn_location = spawn_location_map

    # TODO fix this so ghosts don't teleport
    # TODO implement A*

    def get_random_direction(self) -> tuple:
        LEFT, RIGHT, UP, DOWN = (-VEL, 0), (VEL, 0), (0, -VEL), (0, VEL)
        options = [LEFT, RIGHT, UP, DOWN]

        self.x_vel, self.y_vel = options[random.randint(0, 3)]


def set_direction(keys_pressed, orig_x, orig_y):
    x_vel = 0
    y_vel = 0

    if keys_pressed[pygame.K_a]:  # LEFT
        x_vel = -VEL
        y_vel = 0
    elif keys_pressed[pygame.K_d]:  # RIGHT
        x_vel = VEL
        y_vel = 0
    elif keys_pressed[pygame.K_w]:  # UP
        x_vel = 0
        y_vel = -VEL
    elif keys_pressed[pygame.K_s]:  # DOWN
        x_vel = 0
        y_vel = VEL
    else:
        x_vel = orig_x
        y_vel = orig_y
    return x_vel, y_vel


def draw_map(map_gen):
    # map_gen is an array of strings
    x = 0
    y = 0
    global cols
    cols = len(map_gen[0])
    for row in map_gen:
        for col in row:
            if col == "W":
                wall_group.add(Wall((x, y)))
            elif col == "G":
                inactive_ghosts_group.add(Ghost((x, y)))
            elif col == "P":
                pacman_cord = (x, y)
            elif col == "X":
                # ghost spawn location
                ghost_spawn_location.append(x)
                ghost_spawn_location.append(y)
            elif col == "U":
                power_pellet_group.add(Power_pellet((x, y )))
            elif col == " ":
                pellet_group.add(Pellet((x, y)))

            x += GRID_DIMENSIONS
        y += GRID_DIMENSIONS
        x = 0
    return pacman_cord


def createRandomMap():
    #this will build out a random map by picking a top, middle, and bottom piece
    #from a set of dicts{}
    map = []
    top = tops[random.choice(list(tops.keys()))]
    middle = middles[random.choice(list(middles.keys()))]
    bottom = bottoms[random.choice(list(bottoms.keys()))]
    for line in top:
        map.append(line)
    for line in middle:
        map.append(line)
    for line in bottom:
        map.append(line)
    return map

def map_select(tup, map_name):
    #first checks if the map is meant to be random
    if map_name == 'random':
        new_layout = createRandomMap()
    else:
        map_gen = open(os.path.join("maps/" + map_name), "r")
        new_layout = []
        for line in map_gen:
            new_layout.append(line.strip('\n'))
        map_gen.close()

    wall_group.empty()
    pellet_group.empty()
    power_pellet_group.empty()
    active_ghost_group.empty()
    inactive_ghosts_group.empty()
    pacman_group.empty()


    global level_layout
    level_layout = new_layout
    col_count = len(level_layout[0])
    row_count = len(level_layout)
    global GAME_SURFACE
    #fit game_surface to the size of the map
    GAME_SURFACE = pygame.surface.Surface((col_count * GRID_DIMENSIONS, row_count * GRID_DIMENSIONS))


def draw_window(pacman, score_text: int, lives_text: int, highscores: list):
    WIN.fill(BLACK)
    GAME_SURFACE.fill(BLACK)

    score_text = TEXT_FONT.render(f'Score: {score_text}', 1, WHITE)
    lives_text = TEXT_FONT.render(f'LIVES: {lives_text}', 1, WHITE)
    highscore_title = TEXT_FONT.render(f'Highscores', 1, WHITE)
    highscores_y = score_text.get_height()
    count = 1
    global GAME_SURFACE_X, GAME_SURFACE_Y
    GAME_SURFACE_X = WIN.get_width() / 2 - GAME_SURFACE.get_width() / 2
    GAME_SURFACE_Y = WIN.get_height() / 2 - GAME_SURFACE.get_height() / 2
    for highscore in sorted(highscores, reverse=True):
        highscore_text = TEXT_FONT.render(f'{count}: {highscore:{10}}', 1, WHITE)
        count += 1
        x_pos = WIDTH - highscore_text.get_width() -10
        y_pos = highscore_title.get_height() + highscores_y
        WIN.blit(highscore_text, (x_pos, y_pos))
        highscores_y += highscore_text.get_height()
    WIN.blit(score_text, ((GAME_SURFACE_X + GAME_SIZE/2) - score_text.get_width()/2, 0))
    WIN.blit(highscore_title, (WIDTH - highscore_title.get_width() - 10, highscore_title.get_height()))
    WIN.blit(lives_text, (GAME_SURFACE_X, 0))

    wall_group.draw(GAME_SURFACE)
    pellet_group.draw(GAME_SURFACE)
    power_pellet_group.draw(GAME_SURFACE)
    draw_ghosts()
    pacman_group.draw(GAME_SURFACE)



    WIN.blit(GAME_SURFACE, (GAME_SURFACE_X, GAME_SURFACE_Y))
    pygame.display.update()


def draw_ghosts():
    inactive_ghosts_group.draw(GAME_SURFACE)
    active_ghost_group.draw(GAME_SURFACE)


def ghost_collide_wall():
    for ghost in active_ghost_group:
        if ghost.rect.x < 0:
            ghost.rect.x = GAME_SURFACE.get_width()
        if ghost.rect.x > GAME_SURFACE.get_width():
            ghost.rect.x = 0
        wall_check = pygame.sprite.spritecollideany(ghost, wall_group)
        if wall_check:
            if ghost.x_vel > 0:
                ghost.rect.right = wall_check.rect.left
            if ghost.x_vel < 0:
                ghost.rect.left = wall_check.rect.right
            if ghost.y_vel > 0:
                ghost.rect.bottom = wall_check.rect.top
            if ghost.y_vel < 0:
                ghost.rect.top = wall_check.rect.bottom
            ghost.get_random_direction()


def collide_wall(pacman, collided, x_vel, y_vel):
    if x_vel > 0:
        pacman.rect.right = collided.rect.left
    if x_vel < 0:
        pacman.rect.left = collided.rect.right
    if y_vel > 0:
        pacman.rect.bottom = collided.rect.top
    if y_vel < 0:
        pacman.rect.top = collided.rect.bottom


def collide_pellet(pacman, collided, x_vel, y_vel):
    ret = 0
    if x_vel > 0:
        pacman.right = collided.rect.left
        ret = 1
    if x_vel < 0:
        pacman.left = collided.rect.right
        ret = 1
    if y_vel > 0:
        pacman.bottom = collided.rect.top
        ret = 1
    if y_vel < 0:
        pacman.top = collided.rect.bottom
        ret = 1
    pellet_group.remove(collided)

    return ret


def collision_detect(pacman, x_vel, y_vel, score, lives, pacman_coord):
    wall_check = pygame.sprite.spritecollideany(pacman, wall_group)
    pellet_check = pygame.sprite.spritecollideany(pacman, pellet_group)
    ghost_check = pygame.sprite.spritecollideany(pacman, active_ghost_group)
    power_pellet_check = pygame.sprite.spritecollideany(pacman, power_pellet_group)
    if wall_check:
        collide_wall(pacman, wall_check, x_vel, y_vel)
    if pellet_check:
        score += collide_pellet(pacman, pellet_check, x_vel, y_vel)
        #PACMAN_CHOMP_SOUND.stop()
    if ghost_check:
        # lives -= collide_ghost(pacman, ghost_check, pacman_coord)
        # maybe raise event and then put it on a cool down so collide can only happen succesfully after a second cooldown
        ghost = ghost_check
        if ghost.scared == True:
            PACMAN_EAT_GHOST_SOUND.play()
            score += 50
            active_ghost_group.remove(ghost)
            ghost.rect.x = ghost.initial_x
            ghost.rect.y = ghost.initial_y
            inactive_ghosts_group.add(ghost)
        else:
            PACMAN_DEATH_SOUND.set_volume(.2)
            PACMAN_DEATH_SOUND.play()

            pacman.rect.x, pacman.rect.y = pacman_coord
            lives -= 1

    if power_pellet_check:
        # powers up pacman, makes ghosts scared, reverted in a userevent called from powered_up
        score += 10
        power_pellet_group.remove(power_pellet_check)
        pacman.power_up()

    return score, lives


def collision_detect_ghost():
    for ghost in active_ghost_group:
        wall_check = pygame.sprite.spritecollideany(ghost, wall_group)
        if wall_check:
            if ghost.x_vel > 0:
                ghost.rect.right = wall_check.rect.left
            if ghost.x_vel < 0:
                ghost.rect.left = wall_check.rect.right
            if ghost.y_vel > 0:
                ghost.rect.bottom = wall_check.rect.top
            if ghost.y_vel < 0:
                ghost.rect.top = wall_check.rect.bottom

def save_highscore(score: int):
    with open('highscores.txt', 'a') as highscores:
        highscores.write(str(score) + '\n')

def overwrite_scores(highscores: list):
    highscores = sorted(highscores, reverse=True)
    highscores = highscores[:9]
    with open('highscores.txt', 'w') as hs:
        for score in highscores:
            hs.write(str(score) + '\n')

def run_game():
    game_running = True
    x_vel = 0
    y_vel = 0
    score = 0
    lives = 3

    scores_file = open('highscores.txt', 'r')
    scores_data = scores_file.read()
    highscores = []
    for line in scores_data.split('\n'):
        if line != '':
            highscores.append(int(line))
    scores_file.close()

    pygame.time.set_timer(ghost_status, 1000)
    active_ghost_group.empty()
    inactive_ghosts_group.empty()
    pacman_cord = draw_map(level_layout)
    pacman = Pacman(pacman_cord)
    pacman_group.empty()
    pacman_group.add(pacman)

    # create groups
    #PACMAN_INTRO_SOUND.play()
    #pygame.time.wait(4000)
    PACMAN_CHOMP_SOUND.play(-1) #Waka Waka sound looping continously
    PACMAN_CHOMP_SOUND.set_volume(0.1)


    while game_running:
        # game fps
        clock.tick(FPS)

        # end game on no more lives
        if lives == 0:
            PACMAN_CHOMP_SOUND.stop()
            loss_text = TEXT_FONT.render("Game over! Score: " + str(score), True, RED)
            loss_rect = loss_text.get_rect(center=(WIN.get_width() / 2, WIN.get_height() / 2))
            if len(highscores) < 10:
                save_highscore(score)
            else:
                highscores.append(score)
                overwrite_scores(highscores)
            WIN.blit(loss_text, loss_rect)
            pygame.display.update()
            pygame.time.wait(2000)
            main()

        # win condition
        if not pellet_group:
            PACMAN_CHOMP_SOUND.stop()
            win_text = TEXT_FONT.render("You win! Score: " + str(score), True, RED)
            win_rect = win_text.get_rect(center=(WIN.get_width() / 2, WIN.get_height() / 2))
            if len(highscores) < 10:
                save_highscore(score)
            else:
                highscores.append(score)
                overwrite_scores(highscores)


            WIN.blit(win_text, win_rect)
            pygame.display.update()
            pygame.time.wait(2000)
            pacman_cord = draw_map(level_layout)

        # event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                # user quits game
                game_running = False
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                # this makes up/down movements easier to pull off, tries to lock pacman to closest grid square
                # TODO dont stop movement if a turn is not actually made, (pushing w with a wall directly above pacman)

                if event.key == pygame.K_w:
                    pacman.rotate(90)
                    grid_offset = round(pacman.rect.x / GRID_DIMENSIONS, 0)
                    pacman.rect.x = grid_offset * GRID_DIMENSIONS
                elif event.key == pygame.K_s:
                    pacman.rotate(270)
                    grid_offset = round(pacman.rect.x / GRID_DIMENSIONS, 0)
                    pacman.rect.x = grid_offset * GRID_DIMENSIONS
                elif event.key == pygame.K_a:
                    pacman.rotate(180)
                    grid_offset = round(pacman.rect.y / GRID_DIMENSIONS, 0)
                    pacman.rect.y = grid_offset * GRID_DIMENSIONS
                elif event.key == pygame.K_d:
                    pacman.rotate(0)
                    grid_offset = round(pacman.rect.y / GRID_DIMENSIONS, 0)
                    pacman.rect.y = grid_offset * GRID_DIMENSIONS
                if event.key == pygame.K_ESCAPE:
                    game_running = False
                    pygame.mixer.stop()
                    main()

            if event.type == power_down:
                # reverts status from power pellet after a delay in Pacman class
                pacman.powered_up = False
                #pacman.image = pygame.image.load("images/pacman.png")
                #pacman.image = pygame.transform.scale(pacman.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))

                print("powered down")
                for ghost in active_ghost_group:
                    ghost.scared = False
                    ghost.image = pygame.image.load("images/ghost.png")
                    ghost.image = pygame.transform.scale(ghost.image, (GRID_DIMENSIONS, GRID_DIMENSIONS))
            # release ghost from cage if there are some in there, on a timer
            if event.type == ghost_status:
                if inactive_ghosts_group:
                    all_sprites = inactive_ghosts_group.sprites()
                    sel_ghost = all_sprites.pop()
                    inactive_ghosts_group.remove(sel_ghost)
                    sel_ghost.set_spawn((ghost_spawn_location[0], ghost_spawn_location[1]))
                    sel_ghost.cycle_activation()



        # everything movement related
        keys_pressed = pygame.key.get_pressed()
        score, lives = collision_detect(pacman, x_vel, y_vel, score, lives, pacman_cord)
        x_vel, y_vel = set_direction(keys_pressed, x_vel, y_vel)
        ghost_collide_wall()

        pacman.rect.x += x_vel
        pacman.rect.y += y_vel

        # TODO put teleport things in a function
        active_ghost_group.update()
        pacman_group.update()

        right_side = cols * GRID_DIMENSIONS
        if pacman.rect.x < 0:
            pacman.rect.right = right_side

        if pacman.rect.right > right_side:
            pacman.rect.x = 0
        draw_window(pacman, score, lives, highscores)


def main():
    # will be used in selector menu to choose which map to build
    list_of_files = [x for x in os.listdir("./maps") if x.endswith(".txt")]
    print(list_of_files)
    maps_for_menu = []
    for file in list_of_files:
        maps_for_menu.append((file.removesuffix(".txt"), file))
    #allows selection of a randomly generated map
    maps_for_menu.append(("Generate Random", 'random'))

    # TODO user event where menu brought up again on ESCAPE key usage
    menu = pygame_menu.Menu(
        theme=pygame_menu.themes.THEME_BLUE,
        height=300,
        title='Welcome',
        width=400
    )

    # selector will be for map selection
    map_drop_select = menu.add.dropselect(
        title='Map:',
        items=maps_for_menu,
        onchange=map_select
    )
    menu.add.button('Play', run_game)
    menu.add.button('Quit', pygame_menu.events.EXIT)

    menu.mainloop(surface=WIN)


if __name__ == "__main__":
    main()
