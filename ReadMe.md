# How to Create your own Pac-man game map

* W = wall
* P = pac-man spawn point
  * This will be where pac-man spawns at the start of the game and where he spawns upon death
* G = Ghost spawn point
  * This is where the ghosts will spawn in you will need one per ghost
* X = Ghost activation spawn
  * This is where the ghosts will move to once they become active and spawn in as obstacles for 
    the player
* Blank space/" " = pellets
  * Any space in the game map that is just empty is where a regular pac-man pellet will be drawn
* U = Power Pellet
  * This is where power-up pellets will be drawn for pac-man to eat
* b or any other character not listed = Blank map space
  * If there is an area you intentionally wish to leave blank then use b to indicate that

* Size = 20 columns x 24 rows
  * This is the standard size for a game map to ensure it looks good on the pygame screen

## How to bring all the elements together
 Using the above legend you can write out your own pac-man game map, if you wish to have 
 teleporters from one side of the screen to the other just leave an empty path on the left and 
 right sides

* Be sure to include at least one P, G, X space so pac-man and the ghosts will be drawn and work 
  correctly

Write out your game map in a .txt file starting from the top line 

## Example Map
    WWWWWWWWWWWWWWWWWWWW
    WU        W       UW
    W WW WWWW W WWW WW W
    W WW WWWW W WWW WW W
    W                  W
    W WW W WWWWWWW W W W
    W    W     W   W W W
    WWWW WWWWW W WWW W W
    bbbW W    X    W W W
    bbbW W WWWWWWW W W W
    WWWW W WGbGbGW W W W
           WbbbbbW      
    WWWW W WWWWWWW W WWW
    bbbW W    P    W Wbb
    bbbW W WWWWWWW W Wbb
    WWWW W WWWWWWW W WWW
    W         W        W
    W WW WWWW W WWW WW W
    W  W            WW W
    WW W WWWW W WWW WW W
    W    WWWW W WWW    W
    W WWWWWWW W WWWWWW W
    WU                UW
    WWWWWWWWWWWWWWWWWWWW

## Adding it to the game
Simply put your .txt file containing your map in the main game folder alongside the main.py 
file when you start a new game your map will be listed in the map select box as 'Your_map_name' 
where Your_map_name is whatever you saved the file as minus the .txt

# Random Map Generation
The random map generator works by splitting the map into 3 pieces the top 8 rows, middle 8 rows 
and bottom 8 rows. If you wish you can add in additional pieces of your own design that will 
then be able to be pulled in and used when randomly creating maps.

To do this open the map_layouts.py file and add in your new pieces to the dictionaries already there
The format should look like 

    x: []
Where the x is a key used to reference that part of the map, currently the standard is to just 
use the next integer after the last one in the dictionary, and the [] will contain the map 
piece with each row in single quotes '' and separated by a comma

* Be sure to leave the bottom row of the top piece and the top row of the bottom piece mostly 
  open to avoid sealing off a section
* The ghost cage, pac-man spawn and ghost spawns should all go in the middle section to avoid 
  duplicates
    
